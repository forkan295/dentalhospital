
@extends('admin.pages.main')
@section('content')
    <form role="form" method="POST" action="{{ route('doctor.update',$item->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
                <div class="row">
                    <div class="col-12 col-md-6 mt-4">
                        <div class="card">
                            <div class="card-header">
                            <h3 class="card-title"></h3>
                            </div>
                                
                            <div class="card-body">
                                <div class="form-group">
                                <label for="exampleInputEmail1">Name</label>
                                <input type="text" class="form-control" placeholder="Enter name" name="name" value="{{$item->name}}">
                                @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                </div>
                                <hr class="bg-white">
                                <div class="form-group">
                                    <label for="my-input">Photo</label>
                                    <input id="my-input" class="form-control-file" type="file" name="image" value="{{ $item->image }}">
                                    @error('image')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <hr class="bg-white">
                
                                <div class="form-group" data-select2-id="63">
                                    <label>Speciality</label>
                                    <div class="select2-purple" data-select2-id="38">
                                    <select class="select2 select2-hidden-accessible" name="categories[]" multiple="" data-placeholder="Select a State"  style="width: 100%;"  tabindex="-1" aria-hidden="true">
                                        @foreach ($categories as $cat)
                                            <option value="{{ $cat->id }}"
                                            @foreach ($item->categories as $category)
                                                {{ $category->id == $cat->id ? "selected" : "" }}
                                            @endforeach 
                                            >{{ $cat->name }}</option>
                                        @endforeach  
                                    </select>
                                    </div>
                                    @error('categories')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <hr class="bg-white">
                
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" rows="3" placeholder="Enter ..." name="desc">{{ $item->desc }}</textarea>
                                    @error('desc')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <hr class="bg-white">
                                <div class="form-group">
                                    <label for="my-input">Degree</label>
                                    <input id="my-input" class="form-control" type="text" name="degree" value="{{ $item->degree }}">
                                    @error('degree')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <hr class="bg-white">
                                <div class="form-group">
                                    <label>Education</label>
                                    <textarea class="form-control" rows="1" placeholder="Enter ..." name="education">{{ $item->education }}</textarea>
                                    @error('education')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <hr class="bg-white">
                
                                <div class="form-group">
                                    <label>Experiences</label>
                                    <textarea class="form-control" rows="1" placeholder="Enter ..." name="experiences">{{ $item->Experiences }}</textarea>
                                    @error('experiences')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    
                                </div>
                                
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                    <div class="col-12 col-md-6 mt-4">
                        <div class="card">
                            <div class="card-header">
                                
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="my-input">Address</label>
                                    <input id="my-input" class="form-control" type="text" name="address" value="{{ $item->address }}">
                                    @error('address')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <hr class="bg-white">
                                <div class="form-group">
                                    <label for="my-input">Phone</label>
                                    <input id="my-input" class="form-control" type="text" name="phone" value="{{ $item->phone }}">
                                    @error('phone')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <hr class="bg-white">
                                <div class="form-group">
                                    <label for="my-input">Email</label>
                                    <input id="my-input" class="form-control" type="email" name="email" value="{{ $item->email }}">
                                    @error('email')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <hr class="bg-white">
                                <div class="form-group">
                                    <label for="my-input">Website</label>
                                    <input id="my-input" class="form-control" type="text" name="website" value="{{ $item->website }}">
                                    @error('website')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <hr class="bg-white">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="customSwitch{{ $item->id }}" name="status" {{ $item->status == 1 ? "checked": "" }}>
                                    <label class="custom-control-label" for="customSwitch{{ $item->id }}">Status</label>
                                </div>
                                
                            </div>
                
                        </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-outline-light">Update</button>
                </div>
    </form>
@endsection