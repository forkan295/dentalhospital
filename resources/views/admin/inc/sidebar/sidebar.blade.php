<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      {{-- <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8"> --}}
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          {{-- <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image"> --}}
        </div>
        <div class="info">
          <h4 class="d-block text-white">{{ Auth::user()->name }}</h4>
          <a href="{{ route('logout') }}" class="btn btn-outline-info">Logout</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{route("category.home")}}" class="nav-link  {{ (request()->is('dashboard')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-th"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item">
            
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link  {{ (request()->is('admin/appointment')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-calendar-check"></i>
              <p>
                Appointment
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="display:{{ (request()->is('admin/appointment')) ? 'block' : 'none' }}">
              <li class="nav-item">
                <a href="{{route("appointment.index")}}" class="nav-link  {{ (request()->is('admin/appointment')) ? 'active' : '' }}">
                  <i class="nav-icon far fa-circle"></i>
                  <p>All Appointment</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link  {{ (request()->is('admin/doctor*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-user-md"></i>
              <p>
                Doctors
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="display:{{ (request()->is('admin/doctor*')) ? 'block' : 'none' }}">
              <li class="nav-item">
                <a href="{{route("doctor-category.index")}}" class="nav-link  {{ (request()->is('admin/doctor-category')) ? 'active' : '' }}">
                  <i class="nav-icon far fa-circle"></i>
                  <p>Doctors Category</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route("doctor.index")}}" class="nav-link  {{ (request()->is('admin/doctor')) ? 'active' : '' }}">
                  <i class="nav-icon far fa-circle"></i>
                  <p>All Doctors</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route("doctor.create")}}" class="nav-link  {{ (request()->is('admin/doctor/create')) ? 'active' : '' }}">
                  <i class="nav-icon far fa-circle"></i>
                  <p>Add Doctor</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link  {{ (request()->is('admin/shedule*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-clock"></i>
              <p>
                Shedule
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="display:{{ (request()->is('admin/shedule*')) ? 'block' : 'none' }}">
              <li class="nav-item">
                <a href="{{route("shedule.index")}}" class="nav-link  {{ (request()->is('admin/shedule')) ? 'active' : '' }}">
                  <i class="nav-icon far fa-circle"></i>
                  <p>All Shedule</p>
                </a>
              </li>
              
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link  {{ (request()->is('admin/about*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-address-card"></i>
              <p>
                About Us
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="display:{{ (request()->is('admin/about*')) ? 'block' : 'none' }}">
              <li class="nav-item">
                <a href="{{route("about.index")}}" class="nav-link  {{ (request()->is('admin/about')) ? 'active' : '' }}">
                  <i class="nav-icon far fa-circle"></i>
                  <p>Who We are</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route("mission.index")}}" class="nav-link  {{ (request()->is('admin/about/mission')) ? 'active' : '' }}">
                  <i class="nav-icon far fa-circle"></i>
                  <p>Mission</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route("administration.index")}}" class="nav-link  {{ (request()->is('admin/about/administration')) ? 'active' : '' }}">
                  <i class="nav-icon far fa-circle"></i>
                  <p>Administration</p>
                </a>
              </li>
              
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link  {{ (request()->is('admin/facilities')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-address-card"></i>
              <p>
                Facilities
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="display:{{ (request()->is('admin/facilities')) ? 'block' : 'none' }}">
              <li class="nav-item">
                <a href="{{route("facilities.index")}}" class="nav-link  {{ (request()->is('admin/facilities')) ? 'active' : '' }}">
                  <i class="nav-icon far fa-circle"></i>
                  <p>All Facilities</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link  {{ (request()->is('admin/dental-college*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-address-card"></i>
              <p>
                Dental Collage
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="display:{{ (request()->is('admin/dental-college*')) ? 'block' : 'none' }}">
              <li class="nav-item">
                <a href="{{route("category.index")}}" class="nav-link  {{ (request()->is('admin/dental-college/category')) ? 'active' : '' }}">
                  <i class="nav-icon far fa-circle"></i>
                  <p> Categories</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route("post.index")}}" class="nav-link  {{ (request()->is('admin/dental-college/post')) ? 'active' : '' }}">
                  <i class="nav-icon far fa-circle"></i>
                  <p> Posts</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>