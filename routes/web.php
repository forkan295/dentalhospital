<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout');




            
Route::get('/', function () {
    // return view('admin.pages.dashboard.dashboard');
    return "this is front end";
})->middleware('auth');
Route::get('dashboard','Admin\DashboardController@index')->name('category.home')->middleware('auth');
// Route::get('admin/cat/up/{id}','Admin\CategoryController@catup')->name('cat.up');

Route::prefix('admin')->group(function(){
    Route::group(['middleware' => 'auth'],function () {
        Route::resource('doctor-category', 'Admin\CategoryController');
        Route::resource('appointment', 'Admin\AppointmentController');
        Route::resource('doctor', 'Admin\DoctorController');
        Route::resource('shedule', 'Admin\SheduleController');
        Route::resource('about/mission', 'Admin\aboutus\MissionController');
        Route::resource('about/administration', 'Admin\aboutus\AdministrationController');
        Route::resource('about', 'Admin\aboutus\AboutController');
        Route::resource('facilities', 'Admin\FacilityController');
        Route::resource('dental-college/category', 'Admin\Dental_college\DentalCategoryController');
        Route::resource('dental-college/post', 'Admin\Dental_college\DentalPostController');
    });
});
   

